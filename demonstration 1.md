> Propriété: Une union contient au plus un nœud 'type' qui n'est pas l'alias d'un scalaire.

Démonstration par l'absurde:  
On veut démontrer qu'une union peut contenir plus d'un nœud 'type' qui n'est pas un alias.

On prend donc un nœud union (UAB) composé de k∈ℕ nœuds 'type' donc au moins 2 ne sont pas
des alias.  
On commence par regarder 2 nœuds TA et TB.

On a TA et TB qui possèdent `n`∈ℕ clés en commun, on les nomme {c<sub>0</sub>, ..., c<sub>n</sub>}  
De plus TA possède `m`∈ℕ clés uniques, on les nomme {a<sub>0</sub>, ..., a<sub>m</sub>} (avec n + m ≥ 1)  
Et TB possède `m'`∈ℕ clés uniques, on les nomme {b<sub>0</sub>, ..., b<sub>m'</sub>} (avec n + m' ≥ 1)  

On peut le représenter avec le schéma suivant :

![Tree_graph_Union](img/tree_graph_union.png)

Dans le schéma, les nœuds {va<sub>0</sub>, va<sub>m</sub>}, {ca<sub>0</sub>, ca<sub>n</sub>}, {vb<sub>0</sub>, vb<sub>m'</sub>} et {cb<sub>0</sub>, cb<sub>n</sub>} peuvent être des nœuds 'type' ou des feuilles. De plus rien n'indique que ca<sub>x</sub> est différent de cb<sub>x</sub> (avec 0 ≤ x ≤ n)

Cependant, on peut réduire ces deux types à un seul.  
Prenons donc le type TAB composé des clés uniques de TA et TB. 
Puis pour les values des clés en commun, ssi elles sont différentes, 
on crée une union contenant ces deux valeurs. Tq Uc<sub>0</sub> = [va<sub>0</sub>, vb<sub>0</sub>].
(Ici on suppose toutes les valeurs d'une même clé sont différentes)

![Tree_graph_Union](img/tree_graph_unified.png)

Il est donc possible de fusionner 2 nœuds 'type' non alias de scalaire en un seul nœud 'type'.  
Donc s'il est possible de le faire avec 2 nœuds, on peut le faire avec k nœuds en répétant successivement l'opération autant de fois que nécéssaire.

CQFD

Pourquoi on ne peut pas le faire avec des nœuds 'type' alias de scalaire ?
Pour 2 raisons :
- plus lisible de séparer les alias entre eux et par rapport aux autres nœuds.
- parce que l'union de deux alias de scalaire reviendrait à faire la même chose
  à l'infini. (Puisque l'union de deux alias de scalaires donnerait un type contenant
  un seul champ `_value` ayant pour valeur une union de scalaire ce qui serait remplacé
  par une union d'alias de scalaire : L'état initial avec des branches en plus)