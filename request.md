### Server creation

Took : 38 seconds

### Request few data
```
Took : >1 second
query MyQuery {
  CONTEXTASSETSCollection {
    labelEC
    Id {
      context
      id_
    }
  }
}
```

### Request many data
```
Took : 1 minute
query MyQuery {
  ASSETSCollection {
    Id
    context
    name
    objectType
    values {
      ASLG00001
      ASLG00002
      ASLG00003
      ASLG00004
      ASLG00005
      ASLG00006
      ASLG00009 {
        _value
      }
      ASLG00010
      ASLG00012
      ASLG00013
      ASLG00015
      ASLG00018
      ASLG00020
      ASLG00024
      ASLG00021
      ASLG00026
      ASLG00031
      ASLG00039
      CALCULPRODUITIDDANSASSET
      ProduitId45
    }
  }
}

```
### Request with fragment
```
Took : >2 seconds
query MyQuery {
  PRODUCTSCollection {
    technicalData {
      LF022117 {
        ...str
      }
    }
  }
}
fragment str on StrBuiltin {
  string: _value
}
```
### Request with anonyme fragment (useless)
```
Took : 2 seconds
query MyQuery {
  PRODUCTSCollection {
    ... on ProductsCollection {
      Id
    }
  }
}
```
### Request with anonyme fragment (useful)
```
Took : 3 seconds
query MyQuery {
  PRODUCTSCollection {
    technicalData {
      EFLG000040 {
        ... on FloatBuiltin {
          float: _value
        }
        ... on StrBuiltin {
          string: _value
        }
      }
    }
  }
}
```
### Request with filter
```
Took : >1 second
query MyQuery {
  PRODUCTSCollection {
    Id(like: "^AR")
  }
}

```