# If no server present then clone a default one
[ -f "${PYTHON_DIR}/server.py" ] || cp "${PYTHON_DIR}/empty_server.py" "${PYTHON_DIR}/server.py"

echo "Waiting ${DELAY_BEFORE_START} seconds..." &&
  sleep "${DELAY_BEFORE_START}" &&
  sh "${SCRIPT_DIR}/update.sh" &
  cd "${PYTHON_DIR}" &&
  strawberry server server
