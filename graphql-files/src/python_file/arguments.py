"""
In GraphQL an argument is used to filter results.
Ex:
```graphql
query Query {
    field(filter1: value1, filter2: value2) {
        sub_field(other_filter: other_value)
    }
}
```

Here are the available fields that are automatically generated
for the builtins types (float, int and string).
The key is what the user sees in GraphQL and the value
is the key used in mongodb for this filter with a function
to cast the value given by the user to the correct value.
"""

__all__ = ['general_arguments', 'string_arguments', 'arguments']


general_arguments = {
    'lt': ('$lt', float),
    'lte': ('$lte', float),
    'gt': ('$gt', float),
    'gte': ('$gte', float),
    'eq': ('$eq', float),
    'ne': ('$ne', float),
}

string_arguments = {
    'like': ('$regex', str),
}

arguments = {**general_arguments, **string_arguments}
