from strawberry import field, Schema, type


@type
class Query:
    @field
    def tmp(self) -> str: return 'The server isn\'t up atm'


schema = Schema(query=Query)
