from os import environ
from typing import Iterable

from pymongo import MongoClient
from pymongo.database import Database

__all__ = ['get_client', 'get_database', 'find']


def get_client(**kwargs) -> MongoClient:
    db_name = kwargs.get('db_name', environ.get('MONGODB_DB_NAME', 'test'))
    host = kwargs.get('host', environ.get('MONGODB_HOST', 'mongo'))
    port = kwargs.get('port', environ.get('MONGODB_PORT', '27017'))
    user = kwargs.get('user', environ.get('MONGODB_USER'))
    pwd = kwargs.get('pwd', environ.get('MONGODB_PWD'))
    if user and pwd:
        return MongoClient('mongodb://{0}:{1}@{2}:{3}/?authSource={4}'.format(user, pwd, host, port, db_name))
    return MongoClient('mongodb://{0}:{1}'.format(host, port))


def get_database(**kwargs) -> Database:
    db_name = kwargs.get('db_name', environ.get('MONGODB_DB_NAME', 'test'))
    return get_client(**kwargs)[db_name]


def find(db_name: str, collection: str, filters: dict = None, projection: dict = None) -> Iterable:
    if db_name in {'admin', 'local'}:
        return list()
    filters = filters or dict()
    projection = projection or dict()

    col = get_database()[collection]
    if len(projection) == 0:
        return col.find(filters, {'_id': 0})
    return col.find(filters, projection)
