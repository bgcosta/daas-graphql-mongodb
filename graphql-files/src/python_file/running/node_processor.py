from typing import Union

from graphql import NameNode, VariableNode, NullValueNode, FieldNode, FragmentSpreadNode, ArgumentNode, \
    FragmentDefinitionNode, ValueNode, SelectionSetNode, InlineFragmentNode, SelectionNode

from arguments import arguments as args

__all__ = ['ProcessorData', 'on_fieldNode', 'computeFiltersAndProjection', 'on_selectionSetNode']


class ProcessorData:
    aliases: dict
    fragments: dict
    variables: dict

    def __init__(self, **kwargs):
        self.variables = kwargs.get('variables', dict())
        self.fragments = kwargs.get('fragments', dict())
        self.aliases = kwargs.get('aliases', dict())


def getAlias(node: NameNode, processor_data: ProcessorData) -> str:
    name = on_nameNode(node)
    if name.startswith('_') and name != '__typename':
        return '$'
    return processor_data.aliases.get(name, name)


def on_nameNode(node: NameNode) -> str:
    return node.value


def on_selectionSetNode(node: SelectionSetNode, processor_data: ProcessorData) -> Union[dict, int]:
    if node is None:
        return 1
    values = dict()
    for n in node.selections:
        values.update(on_selectionNode(n, processor_data))
    return values


def on_selectionNode(node: SelectionNode, processor_data: ProcessorData) -> dict:
    if isinstance(node, FieldNode):
        return on_fieldNode(node, processor_data)
    if isinstance(node, InlineFragmentNode):
        return on_inlineFragmentNode(node, processor_data)
    if isinstance(node, FragmentSpreadNode):
        return on_fragmentSpreadNode(node, processor_data)
    return dict()


def on_fieldNode(node: FieldNode, processor_data: ProcessorData) -> dict:
    return {
        getAlias(node.name, processor_data): {
            '__value__': on_selectionSetNode(node.selection_set, processor_data),
            '__args__': on_argumentNodes(node.arguments, processor_data)
        }
    }


def on_argumentNodes(nodes: list[ArgumentNode], processor_data: ProcessorData) -> dict:
    return {on_nameNode(node.name): on_valueNode(node.value, processor_data) for node in nodes}


def on_fragmentSpreadNode(node: FragmentSpreadNode, processor_data: ProcessorData) -> dict:
    name = on_nameNode(node.name)
    if isinstance(processor_data.fragments[name], FragmentDefinitionNode):
        processor_data.fragments[name] = on_fragmentDefinitionNode(processor_data.fragments[name], processor_data)[name]
    return processor_data.fragments[name]


def on_inlineFragmentNode(node: InlineFragmentNode, processor_data: ProcessorData) -> dict:
    return on_selectionSetNode(node.selection_set, processor_data)


def on_fragmentDefinitionNode(node: FragmentDefinitionNode, processor_data: ProcessorData) -> dict:
    return {getAlias(node.name, processor_data): on_selectionSetNode(node.selection_set, processor_data)}


def on_valueNode(node: ValueNode, processor_data: ProcessorData) -> any:
    if isinstance(node, VariableNode):
        return processor_data.variables.get(on_nameNode(node.name), '???')
    if isinstance(node, NullValueNode):
        return None
    return getattr(node, 'value', None)


def computeFiltersAndProjection(parsed_query: dict) -> tuple[dict, dict]:
    filters, projections = dict(), dict()

    def on_arguments(arguments: dict, parent_key: str):
        values = dict()
        for k, v in arguments.items():
            key, cast = args[k]
            values[key] = cast(v)
        if len(values) != 0:
            filters[parent_key] = values

    def on_objects(current_node: dict, parent_key: str = ''):
        if '$' in current_node:
            projections[parent_key] = 1
            for k, v in current_node.items():
                if k == '$':
                    on_arguments(v.get('__args__'), parent_key)
                else:
                    on_arguments(v.get('__args__'), f'{parent_key}.{k}')
        else:
            for k, v in current_node.items():
                if k != '__typename':
                    on_object(v, f'{parent_key}.{k}')

    def on_object(current_node: dict, parent_key: str = ''):
        on_arguments(current_node.get('__args__'), parent_key)
        if not isinstance(value := current_node.get('__value__'), int):
            return on_objects(value, parent_key)
        projections[parent_key] = 1

    collection_name, data = parsed_query.popitem()
    for k, v in data.get('__value__').items():
        on_object(v, k)
    return filters, projections
