from os import environ
from typing import Iterable, Optional

from strawberry.types import Info

from mongo_client import find
from running.node_processor import ProcessorData, on_fieldNode, computeFiltersAndProjection

__all__ = ['instantiate_type', 'as_type', 'if_instance', 'on_list', 'request_to_database']


#  For server's types
def instantiate_type(data: any, t: any) -> any:
    """
    :param data: the data to initiate the given type.
    :param t: the output type.
    :return: a new object of the given type or
        None if the data contains a key not in the type.
    """
    if len(t.keys) == 0 and isinstance(data, t.alias):
        return t(data)
    if len(tuple(x for x in data.keys() if x not in t.keys)) == 0:
        return t(data)


def as_type(obj: any, t: any) -> any:
    """
    Returns the given object as the given type.

    :param obj: the input object
    :param t: the output type
    :return: the same object passed into the default constructor of the type.
        If the object cannot be converted return None.
    """
    if isinstance(obj, t) or obj is None:
        return obj
    try:
        return instantiate_type(obj, t)
    except Exception:
        return None


#  For builtins
def if_instance(obj: any, t: type) -> any:
    """
    Returns the given object if it's type match the given type

    :param obj: the input object
    :param t: the output type
    :return: None if the object isn't an instance of the given type; the object untouched otherwise.
    """
    if obj is None:
        return None
    if isinstance(obj, t):
        return obj
    return None


def on_list(data: Optional[list], return_types: tuple[type]) -> list:
    if data and hasattr(data, '__iter__'):
        return [x for d in data if (x := first_defined(d, return_types))]


def first_defined(data: any, return_types: tuple) -> any:
    for t in return_types:
        if d := as_type(data, t) is not None:
            return d


#  Make request
async def request_to_database(info: Info, collection: str, aliases: dict) -> Iterable:
    processor_data = ProcessorData(fragments={**info.fragments}, variables=info.variable_values, aliases=aliases)
    parsed_query = on_fieldNode(info.field_nodes[0], processor_data)
    filters, projection = computeFiltersAndProjection(parsed_query)
    collection = aliases.get(collection, collection)
    return find(environ['MONGODB_DB_NAME'], collection, filters=filters, projection=projection)
