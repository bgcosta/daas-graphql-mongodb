from __future__ import annotations

from typing import Optional, Union

from creation.counter import Counter

__all__ = ['Schema']


class Schema:
    def __init__(self):
        self.keys = set()
        self.list = set()
        self.types = dict()
        self.union = dict()
        self.aliases = dict()
        self.builtins = dict()
        self.counter = Counter()
        self.collections = set()

    # Utils

    @staticmethod
    def get_resolver_for_key(key: str) -> str:
        new_key = ''.join(x for x in key if x.isalnum() or x == '_')
        if key[0].isdecimal():
            return '_' + new_key
        return new_key

    def get_type_from_union(self, key: str) -> Optional[str]:
        for x in self.union[key]:
            if x not in self.builtins.values():
                return x
        return None

    def correct_key(self, key: str) -> str:
        """
        :returns: the input key with trailing 'Sub' if the key is already in the types
        """
        new_key = key
        while new_key in self.keys:
            new_key = key + '_' + str(self.counter.inc_and_get(key))
        return new_key

    # New elements

    def add_type(self, key: str, value: dict[str, any]) -> str:
        """
        :returns: the correct key added in the types; Can be different
                  than input if the key was already present
        """
        key = self.correct_key(key)
        self.keys.add(key)
        self.types[key] = value
        return key

    def add_union(self, key: str, value: set[str]) -> str:
        """
        :returns: the correct key added in the union; Can be different
                  than input if the key was already present
        """
        key = self.correct_key(key)
        self.keys.add(key)
        self.union[key] = value
        return key

    def add_builtin(self, builtin: type) -> str:
        """
        :returns: the name of the type of the given builtin
        """
        if (b := self.builtins.get(builtin)) is not None:
            return b
        self.builtins[builtin] = self.add_type(builtin.__name__ + '_builtin', {'value': builtin})
        return self.builtins[builtin]

    def set_list(self, key: str) -> str:
        if key not in self.keys:
            raise Exception('Not existing type or union ' + key)
        self.list.add(key)
        return key

    def set_collection(self, key: str) -> str:
        if key not in self.types.keys():
            raise Exception('Not existing type ' + key)
        self.collections.add(key)
        self.set_list(key)
        return key

    def set_alias(self, key: str, value: str) -> str:
        self.aliases[key] = value
        return key

    # Add elements from other DataHolder

    def transfer_data(self, other: Schema, key: Union[str, type]) -> any:
        """
        :returns: the new value associated with this one. Can be a key or a builtin.
        """
        if key in other.types:
            return self.transfer_type(other, key)
        if (value := other.union.get(key)) is not None:
            if (x := other.get_type_from_union(key)) is not None:
                value.discard(x)
                value.add(self.transfer_type(other, x))
            key = self.add_union(key, value)
        return key

    def transfer_type(self, other: Schema, key: str) -> str:
        """
        :returns: the correct key added in the types; Can be different
                  than input if the key was already present
        """
        new_key = self.add_type(key, other.types[key])
        for k, v in other.types[key].items():
            self.types[new_key][k] = self.transfer_data(other, v)
        return new_key

    # Merge methods

    def merge(self, other: Schema, key: str) -> str:
        self.counter.merge_counter(other.counter)

        for k in other.builtins.keys():
            if k not in self.builtins:
                self.add_builtin(k)
        self.list.update(other.list)

        if key in self.union:
            key = self.merge_values(other, key, key, key)
        elif key in self.types:
            key = self.merge_values(other, key, key, key)
        else:
            key = self.transfer_data(other, key)
        return key

    def merge_dict(self, other: Schema, my: dict, his: dict) -> dict:
        for k, v in his.items():
            if k in my:
                my[k] = self.merge_values(other, my[k], v, k)
            else:
                my[k] = self.transfer_data(other, v)
        return my

    def __on_type(self, other: Schema, my: str, him: any, key: str) -> str:
        if (his_type := other.types.get(him)) is not None:
            self.types[my] = self.merge_dict(other, self.types[my], his_type)
            return my
        if (his_union := other.union.get(him)) is not None:
            if his_subtype := other.get_type_from_union(him):
                self.types[my] = self.merge_dict(other, self.types[my], other.types[his_subtype])
                new_key = self.add_union(him, his_union)
                self.transfer_data(other, his_subtype)
            else:
                new_key = self.add_union(him, {my, *his_union})
            return new_key
        if isinstance(him, type):
            return self.add_union('U_' + key, {my, self.add_builtin(him)})
        return my

    def __on_union(self, other: Schema, my: str, him: any) -> str:
        if (his_type := other.types.get(him)) is not None:
            if (my_sub := self.get_type_from_union(my)) is not None:
                self.types[my_sub] = self.merge_dict(other, self.types[my_sub], his_type)
            else:
                self.union[my].add(self.transfer_type(other, him))
        elif (his_union := other.union.get(him)) is not None:
            s = self.union[my] | his_union
            my_sub = self.get_type_from_union(my)
            his_sub = other.get_type_from_union(him)
            if my_sub and his_sub:
                self.types[my_sub] = self.merge_dict(other, self.types[my_sub], other.types[his_sub])
                if his_sub != my_sub:
                    s.remove(his_sub)
            elif his_sub:
                s.remove(his_sub)
                s.add(self.transfer_data(other, his_sub))
            self.union[my] = s
        elif isinstance(him, type):
            self.union[my].add(self.add_builtin(him))
        return my

    def __on_builtin(self, other: Schema, my: type, him: any, key: str) -> any:
        if him in other.types:
            self.transfer_data(other, him)
            return self.add_union('U_' + key, {him, self.add_builtin(my)})
        if him in other.union:
            kk = self.transfer_data(other, him)
            self.union[kk] = {*self.union[kk], self.add_builtin(my)}
            return kk
        if my == him:
            return my
        return self.add_union('U_' + key, {self.add_builtin(my), self.add_builtin(him)})

    def merge_values(self, other: Schema, my: any, him: any, key: str) -> any:
        if my in self.types:
            return self.__on_type(other, my, him, key)
        if my in self.union:
            return self.__on_union(other, my, him)
        if isinstance(my, type):
            return self.__on_builtin(other, my, him, key)
        return self.transfer_data(other, key)

    # Display : only for debug, uncomment wanted data

    def to_string(self) -> str:
        s = ''
        s += 'types = {' + ', '.join(str(k) + ': ' + str(self.types[k]) for k in sorted(self.types)) + '}\n'
        s += 'union = {' + ', '.join(str(k) + ': ' + str(sorted(self.union[k])) for k in sorted(self.union)) + '}\n'
        # s += 'aliases = {' + ', '.join(str(k) + ': ' + str(self.aliases[k]) for k in sorted(self.aliases)) + '}\n'
        # s += 'builtins = {' + ', '.join(
        #     str(k) + ': ' + str(self.builtins[k]) for k in sorted(self.builtins, key=str)) + '}\n'
        # s += 'list = {' + ', '.join(str(k) for k in sorted(self.list)) + '}\n'
        # s += 'keys = {' + ', '.join(str(k) for k in sorted(self.keys)) + '}\n'
        # s += 'collections = {' + ', '.join(str(k) for k in sorted(self.collections)) + '}\n'
        return s

    def __str__(self) -> str:
        return self.to_string()

    def __repr__(self) -> str:
        return self.to_string()
