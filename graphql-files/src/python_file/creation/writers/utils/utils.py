import re


__all__ = ('PascalCase', 'field_to_graphql')


__ALLOWED_FIELD_CHAR_REGEX = re.compile(r'[a-zA-Z0-9_]')


def PascalCase(s: str) -> str:
    return ''.join(x for x in s.title() if x.isalnum())


def field_to_graphql(key: str) -> str:
    buff = ''.join(c for c in key if __ALLOWED_FIELD_CHAR_REGEX.match(c))

    if buff[0].isdecimal():
        return '_' + buff
    return buff + '_' if buff in __builtins__ else buff
