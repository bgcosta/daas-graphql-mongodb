from typing import Optional

import arguments


# -------------------------------
#             Header
# -------------------------------


def imports() -> str:
    return ('from __future__ import annotations\n'
            '\n'
            'from datetime import datetime\n'
            'from logging import warning as warn\n'
            'from time import time\n'
            'from typing import List, Optional, Union\n'
            '\n'
            'from strawberry import Schema, field as s_field, type as s_type\n'
            'from strawberry.extensions import Extension\n'
            'from strawberry.types import Info\n'
            '\n'
            'from running.utils import as_type, if_instance, request_to_database\n'
            '\n\n')


def header() -> str:
    return imports()


# -------------------------------
#              Body
# -------------------------------


def get_builtin_format(class_name: str, builtin: type) -> str:
    return ('@s_type\n'
            f'class {class_name}:\n'
            '    keys = {}\n'
            f'    alias = {builtin.__name__}\n'
            '\n'
            '    def __init__(self, data):\n'
            '        self.value = data\n'
            '\n'
            '    @s_field(name=\'_value\')\n'
            f'    def value(self, {get_filters(builtin.__name__)}) -> Optional[{builtin.__name__}]:\n'
            '        return self.value\n'
            '\n\n')


def get_class_format(class_name: str, fields: set) -> str:
    return ('@s_type\n'
            f'class {class_name}:\n'
            f'    keys = {fields}\n'
            '\n'
            '    def __init__(self, data):\n'
            )


def get_union(class_name: str, elements: set) -> str:
    elements = ', '.join(x for x in elements)
    return f'{class_name} = Union[{elements}]\n\n\n'


def get_collection_header(class_name: str, fields: set) -> str:
    fields.add('_id')
    return get_class_format(class_name, fields)


# -------------------------------
#            Resolver
# -------------------------------


def get_filters(type_name: str) -> str:
    if type_name == 'str':
        args = arguments.string_arguments
    elif type_name in ['int', 'float']:
        args = arguments.general_arguments
    else:
        return ''

    return ', '.join(f'{k}: Optional[{type_name}] = None' for k in args.keys())


def for_union(union: set, type_name: str, constructor: str, field: Optional[str] = None) -> str:
    data = 'd' if field is None else f'data.get(\'{field}\')'
    if union:
        return ' or '.join(f'as_type({data}, {t})' for t in union)
    return f'{constructor}({data}, {type_name})'


def get_default_resolver(type_name: str, field: str, resolver: str, constructor: str, union: set = None) -> str:
    return f'        self.{resolver} = {for_union(union, type_name, constructor, field)}\n'


def get_list_resolver(type_name: str, field: str, resolver: str, constructor: str, union: set = None) -> str:
    return (f'        self.{resolver} = [x for d in d2 if (x := {for_union(union, type_name, constructor)})]\\\n'
            f'            if (d2 := data.get(\'{field}\')) and hasattr(d2, \'__iter__\') else None\n')


def get_resolver(type_name: str, field: str, resolver: str, constructor: str, is_list: bool, union: set = None) -> str:
    if is_list:
        return get_list_resolver(type_name, field, resolver, constructor, union)
    return get_default_resolver(type_name, field, resolver, constructor, union)


def get_resolver_real(resolver: str, type_name: str, is_list: bool) -> str:
    if is_list:
        return f'    @s_field\n' \
               f'    def {resolver}(self, {get_filters(type_name)}) -> Optional[List[{type_name}]]:\n' \
               f'        return self.{resolver}\n'
    return f'    @s_field\n' \
           f'    def {resolver}(self, {get_filters(type_name)}) -> Optional[{type_name}]:\n' \
           f'        return self.{resolver}\n'


def get_query_resolvers(type_name: str, field: str, resolver: str) -> str:
    return ('    @s_field\n'
            f'    async def {resolver}(self, info: Info) -> List[{type_name}]:\n'
            f'        start = time()\n'
            f'        data = await request_to_database(info=info, collection=\'{field}\', aliases=aliases)\n'
            f'        end = time()\n'
            f'        warn(f\'Mongo Request took {{(end - start) * 1000}} ms\')\n'
            f'        start = time()\n'
            f'        data = [x for d in data if (x := as_type(d, {type_name}))]\n'
            f'        end = time()\n'
            f'        warn(f\'{resolver} data resolving took {{(end - start) * 1000}} ms\')\n'
            f'        return data\n'
            '\n'
            )


def get_constructor(type_name: any) -> str:
    """
    The constructor is `as_type()` if the `type_name` is a string
    or `if_instance` if the `type_name` is a builtin
    """
    if isinstance(type_name, type):
        return 'if_instance'
    return 'as_type'


# -------------------------------
#             Footer
# -------------------------------

def get_query_header() -> str:
    return ('@s_type\n'
            'class Query:\n')


def _aliases(aliases: dict) -> str:
    aliases = {k: v for k, v in aliases.items() if k != v}
    return f'aliases = {aliases}\n'


def _types(types: set) -> str:
    types = ', '.join(x for x in types)
    return f'types = [{types}]\n'


def __extension() -> str:
    return ('class Ext(Extension):\n'
            '    def on_request_start(self):\n'
            '        self.execution_context.start = time()\n'
            '\n'
            '    def on_request_end(self):\n'
            '        self.execution_context.end = time()\n'
            '        warn(f\'Full request took '
            '{(self.execution_context.end - self.execution_context.start) * 1_000} ms\')\n'
            '\n\n')


def __schema() -> str:
    return 'schema = Schema(query=Query, types=types, extensions=[Ext])\n'


def get_footer(aliases: dict, types: set) -> str:
    return '\n' + _aliases(aliases) + _types(types) + '\n\n' + __extension() + __schema()
