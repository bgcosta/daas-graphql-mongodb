
## Points importants

- La commande pour lancer le server GraphQL est : `strawberry server schema`  
  `schema` étant le nom du fichier python contenant le schema.
  [Syntaxe complète](https://strawberry.rocks/docs/guides/server)

- Le serveur peut tourner pendant que le schema est mis à jours
  mais il est indisponible pendant que le schema est en train d'être rechargé.  
  Les requêtes envoyées avant que le schema n'ai commencé à être rechargé seront
  perdue (à verifier mais très probable)

- Le serveur tourne en parallèle du programme de mise à jour

- Il y a plusieurs moyens de mettre le schema à jour :
  - À chaque insertion dans la base, mettre à jour le schema
  - Au bout d'un certain nombre d'insertions
  - Tout les x jours/semaines/mois

- Il faudrait ajouter un petit module permettant de dump et de load le
  schema en format brut (l'objet 'Schema')
  (format json possible si conversion des sets en liste et les builtins en chaines)


## Quelques infos intéressantes

- un champ GraphQL qui contient un '\_' essaiera de mettre
  la lettre qui suite en majuscule et supprimera le '\_'.
  Si le champ commence par '\_\_' alors le nom du parent sera ajouté
  avant le nom du champ et avec un '\_' pour séparer les deux.

- un champ GraphQL doit respecter les mêmes conventions de
  nommage de champs que la plupart des languages ([a-zA-Z_][a-zA-Z0-9_]*)
