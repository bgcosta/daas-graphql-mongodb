from typing import TextIO

from creation.schema import Schema
from . import writer_templates as templates
from ..utils.utils import PascalCase, field_to_graphql


__all__ = ["write"]


def key_to_type_name(key: str) -> str:
    if isinstance(key, type):
        return key.__name__
    return PascalCase(key)


def is_list(key: any, schema: Schema) -> bool:
    return key in schema.list


def write_header(out: TextIO, schema: Schema) -> None:
    out.write(templates.header())
    for k, v in schema.builtins.items():
        out.write(templates.get_builtin_format(key_to_type_name(v), k))


def on_undef_node(out: TextIO, schema: Schema, value: any, not_seen: set[str]) -> None:
    if value in not_seen:
        not_seen.discard(value)
        write_node(out, schema, value, schema.types.get(value) or schema.union.get(value), not_seen)


def write_fields(out: TextIO, schema: Schema, key: str, value: str, pkey: str) -> str:
    type_name, field, resolver = key_to_type_name(value), key, field_to_graphql(key)
    constructor = templates.get_constructor(value)
    union = schema.union.get(value)
    schema.set_alias(resolver, field)
    is_list = value in schema.list
    out.write(templates.get_resolver(type_name, field, resolver, constructor, is_list,
                                     {key_to_type_name(k) for k in union} if union else union))
    return templates.get_resolver_real(resolver, type_name, is_list) + '\n'


def write_type(out: TextIO, schema: Schema, key: str, value: dict, not_seen: set[str]) -> None:
    for k, v in value.items():
        on_undef_node(out, schema, v, not_seen)
    keys = {x for x in value.keys()}
    if key in schema.collections:
        keys.add('_id')

    out.write(templates.get_class_format(key_to_type_name(key), keys))

    buff = ''.join(write_fields(out, schema, k, v, key) for k, v in value.items())

    out.write('\n')
    out.write(buff)
    out.write('\n')


def write_union(out: TextIO, schema: Schema, key: str, value: set, not_seen: set[str]) -> None:
    for v in value:
        on_undef_node(out, schema, v, not_seen)
    out.write(templates.get_union(key_to_type_name(key), {key_to_type_name(x) for x in value}))


def write_node(out: TextIO, schema: Schema, key: str, value: any, not_seen: set[str]) -> None:
    if key in schema.types:
        return write_type(out, schema, key, value, not_seen)
    if key in schema.union:
        return write_union(out, schema, key, value, not_seen)
    raise Exception('Invalid value {} of type {}'.format(value, type(value)))


def write_body(out: TextIO, schema: Schema) -> None:
    not_seen = {*schema.keys} - {*schema.builtins.values()}

    for k, v in schema.types.items():
        if k in not_seen:
            not_seen.discard(k)
            write_node(out, schema, k, v, not_seen)


def write_footer(out: TextIO, schema: Schema) -> None:
    out.write(templates.get_query_header())
    for collection in schema.collections:
        type_name, field, resolver = \
            PascalCase(collection), collection.replace('_collection', ''), field_to_graphql(collection)
        schema.set_alias(resolver, field)
        out.write(templates.get_query_resolvers(type_name, field, resolver))
    types = {key_to_type_name(x) for y in schema.union.values() for x in y}
    out.write(templates.get_footer(schema.aliases, types))


def write(schema: Schema, file_name: str, *args, **kwargs) -> None:
    with open(file_name, 'w') as out:
        write_header(out, schema)
        write_body(out, schema)
        write_footer(out, schema)
