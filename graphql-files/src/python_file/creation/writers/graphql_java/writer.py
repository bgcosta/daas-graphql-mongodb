from typing import TextIO

from creation.schema import Schema
from . import writer_templates as templates
from ..utils.utils import PascalCase, field_to_graphql

# Actual byte code threshold - a arbitrary amount of data (method call for instance)
__JVM_MAX_CODE_LENGTH = 65536 - 1000


def append_text(data: tuple[str, int], out: TextIO, tail: str) -> tuple[str, int]:
    text, index = data
    if len(text) >= __JVM_MAX_CODE_LENGTH - len(tail):
        out.write(templates.wiring_sub_method(text, index))
        index, text = index + 1, f'buildWiring_{index}()\n'
    return text + tail, index


def write_wiring(schema: Schema, out: TextIO) -> str:
    data = templates.wiring_header(), 0
    data = append_text(data, out, templates.wiring_query('Query', schema.collections))
    for k, v in {k: v for k, v in schema.types.items() if k not in schema.builtins.values()}.items():
        data = append_text(data, out, templates.wiring_type(PascalCase(k), (schema.aliases.get(x, x) for x in v)))
    for v in schema.builtins.values():
        data = append_text(data, out, templates.wiring_builtin(PascalCase(v)))
    for k, v in schema.union.items():
        data = append_text(data, out, templates.wiring_union(schema, PascalCase(k), v))

    return data[0] + templates.wiring_footer(schema)


def write_resolver(schema: Schema, out: TextIO):
    text = write_wiring(schema, out)
    out.write(templates.resolver_header())
    out.write(text)
    out.write(templates.resolver_footer())


def write_alias(schema: Schema, out: TextIO):
    out.write(templates.aliases_header())
    out.write(',\n'.join(templates.aliases_format(k, v) for k, v in schema.aliases.items()))
    out.write(templates.aliases_footer())


def write_class(schema: Schema, out: TextIO):
    out.write(templates.class_header(out.name).replace('.java', ''))
    write_alias(schema, out)
    write_resolver(schema, out)
    out.write(templates.class_footer())


def write_schema(schema: Schema, out: TextIO):
    out.write(templates.schema_scalars(schema))
    out.write(templates.schema_type(schema, 'Query', {x: x for x in schema.collections}))
    for k, v in schema.types.items():
        if k not in schema.builtins.values():
            out.write(templates.schema_type(schema, PascalCase(k), v))
    for k, v in schema.union.items():
        out.write(templates.schema_union(PascalCase(k), (PascalCase(x) for x in v)))
    for k, v in schema.builtins.items():
        out.write(templates.schema_builtin(schema, k, PascalCase(v)))


def write(schema: Schema, class_name: str, schema_name: str, *args, **kwargs) -> None:
    for key, values in schema.types.items():
        for k in values.keys():
            k2 = field_to_graphql(k)
            if k2 != k:
                schema.set_alias(k, k2)

    with open(schema_name, 'w') as out:
        write_schema(schema, out)

    with open(class_name, 'w') as out:
        out.write(templates.header())
        write_class(schema, out)

