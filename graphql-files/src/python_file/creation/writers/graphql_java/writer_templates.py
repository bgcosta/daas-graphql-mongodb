import datetime
from typing import Iterable

import arguments
from creation.schema import Schema
from ..utils.utils import PascalCase


# -------------------------------
#          Java File
# -------------------------------

def __package() -> str:
    return 'package fr.cantor.graphql;\n\n\n'


def __imports() -> str:
    return ('import fr.cantor.graphql.runtime.utils.ServerTemplate;\n'
            'import fr.cantor.graphql.runtime.utils.DateScalar;\n'
            'import graphql.schema.idl.RuntimeWiring;\n'
            'import org.bson.Document;\n'
            'import org.springframework.stereotype.Component;\n'
            '\n'
            'import java.util.Date;\n'
            'import java.util.Map;\n'
            'import java.util.stream.Collectors;\n'
            'import java.util.stream.Stream;\n'
            '\n'
            'import static fr.cantor.graphql.runtime.utils.ServerMethods.*;\n'
            'import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;\n'
            '\n')


def header() -> str:
    return __package() + __imports()


# -------------------------------
#              Class
# -------------------------------

def class_header(class_name: str) -> str:
    return ('@Component\n'
            f'public class {class_name} extends ServerTemplate ' '{\n')


def class_footer() -> str:
    return '}'


# -------------------------------
#              Aliases
# -------------------------------

def aliases_header() -> str:
    return '\tprivate static final Map<String, String> aliases = Stream.<Map.Entry<String, String>>of(\n'


def aliases_format(src: str, to: str) -> str:
    return f'\t\t\tMap.entry("{to}", "{src}")'


def aliases_footer() -> str:
    return '\n\t).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));\n\n'


# -------------------------------
#            Resolver
# -------------------------------

def resolver_header() -> str:
    return ('\t@Override\n'
            '\tprotected RuntimeWiring buildWiring() {\n'
            '\t\treturn ')


def resolver_footer() -> str:
    return '\t}\n'


# -------------------------------
#            Wiring
# -------------------------------

def wiring_header() -> str:
    return 'newRuntimeWiring()\n'


def wiring_sub_method(text: str, index: int) -> str:
    if text[-1] == '\n':
        text = text[:-1]
    return (f'\tprivate RuntimeWiring.Builder buildWiring_{index}() ' '{\n'
            f'\t\treturn {text};\n'
            '\t}\n')


def wiring_query(query: str, collections: Iterable[str]) -> str:
    collections = ', '.join(f'"{x}"' for x in collections)
    return f'\t\t\t\t.type(newQueryWiring("{query}", aliases, client, {collections}))\n'


def wiring_type(type_name: str, fields: Iterable[str]) -> str:
    fields = ', '.join(f'"{x}"' for x in fields)
    return f'\t\t\t\t.type(newTypeWiring("{type_name}", aliases, {fields}))\n'


def wiring_builtin(builtin: str) -> str:
    return f'\t\t\t\t.type(newBuiltinWiring("{builtin}"))\n'


def __graphql_type_to_java_type(graphql: str, schema: Schema) -> str:
    for k, v in schema.builtins.items():
        if v == graphql:
            return {str: 'String', int: 'Integer', float: 'Float', datetime.datetime: 'Date'}.get(k)
    return 'Document'


def wiring_union(schema: Schema, union: str, elements: Iterable[str]) -> str:
    elements = ',\n'.join('\t' * 6 + f'new UnionData({__graphql_type_to_java_type(x, schema)}.class, "{PascalCase(x)}")'
                          for x in elements)
    return (f'\t\t\t\t.type(newUnionWiring("{union}",\n' 
            f'{elements}\n' 
            f'\t\t\t\t))\n')


def __wiring_optional_actions(schema: Schema) -> str:  # Modifications to add before build
    s = ''
    return s


def __wiring_build() -> str:
    return ('\t\t\t\t.scalar(new DateScalar("Date"))\n'
            '\t\t\t\t.build();\n')


def wiring_footer(schema: Schema) -> str:
    return __wiring_optional_actions(schema) + __wiring_build()


# -------------------------------
#            Schema
# -------------------------------

def __python_type_to_graphql(python_type: any) -> str:
    return {str: 'String', int: 'Int', float: 'Float', datetime.datetime: 'Date'}.get(python_type)


def __filters_for_field(schema: Schema, key: str, value: any) -> str:
    key = schema.aliases.get(key, key)
    if not isinstance(value, type):
        return key
    if value == str:
        filters = arguments.string_arguments
    elif value in [int, float]:
        filters = arguments.general_arguments
    else:
        return key
    return key + '(' + ', '.join(f'{k}: {__python_type_to_graphql(v)}' for k, (_, v) in filters.items()) + ')'


def __graphql_field_resolving(schema: Schema, key: str, value: any) -> str:
    key = __filters_for_field(schema, key, value)
    if isinstance(value, type):
        return f'\t{key}: {__python_type_to_graphql(value)}'
    if value in schema.list:
        return f'\t{key}: [{PascalCase(value)}]'
    return f'\t{key}: {PascalCase(value)}'


def schema_scalars(schema: Schema) -> str:
    return 'scalar Date\n'


def schema_type(schema: Schema, type_name: str, fields: dict[str, str]) -> str:
    fields = ',\n'.join(__graphql_field_resolving(schema, k, v) for k, v in fields.items())
    return (f'type {type_name} ' '{\n'
            f'{fields}\n'
            '}\n\n')


def schema_union(union: str, elements: Iterable[str]) -> str:
    elements = ' | '.join(elements)
    return f'union {union} = {elements}\n\n'


def schema_builtin(schema: Schema, builtin: type, builtin_name: str) -> str:
    return_type = __graphql_field_resolving(schema, "_value", builtin)
    return (f'type {builtin_name} ' '{\n'
            f'{return_type}\n'
            '}\n\n')
