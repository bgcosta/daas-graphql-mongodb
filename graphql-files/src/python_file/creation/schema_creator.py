from pymongo.database import Database

from creation.schema import Schema

__all__ = ['schema_for_object', 'schema_for_list', 'create_schema_from_db', 'create_schema_from_raw_data']


def schema_for_object(schema: Schema, key: str, value: dict[str, any]) -> str:
    data = dict()
    for k, v in value.items():
        if isinstance(v, dict):
            data[k] = schema_for_object(schema, k, v)
        elif isinstance(v, list):
            list_key = k + '_list'
            data[k] = schema.set_list(schema.merge(*schema_for_list(list_key, v)))
        else:
            data[k] = type(v)
    return schema.add_type(key, data)


def schema_for_builtin(schema: Schema, key: str, value: type) -> str:
    return schema.add_union(key, {schema.add_builtin(value)})


def schema_for_list(list_name: str, values: list[dict]) -> tuple[Schema, str]:
    schema = Schema()
    entry = list_name
    for value in values:
        if isinstance(value, dict):
            key = schema_for_object(s := Schema(), entry, value)
        else:
            key = schema_for_builtin(s := Schema(), entry, type(value))
        entry = schema.merge(s, key)
    return schema, entry


def create_schema_from_db(db: Database, **kwargs) -> Schema:
    collections = kwargs.get('collections', db.list_collection_names())
    schema = kwargs.get('schema', Schema())

    for collection in collections:
        key = collection
        print("Collection :", collection)
        value = [x for x in db.get_collection(collection).find()]
        schema.set_collection(schema.merge(*schema_for_list(key, value)))
    return schema


def create_schema_from_raw_data(collections: dict[str, list], **kwargs) -> Schema:
    collection_names = kwargs.get('collection_names', collections.keys())
    schema = kwargs.get('schema', Schema())
    for k in collection_names:
        collection_key = k
        print("Collection :", k)
        key = schema.merge(*schema_for_list(collection_key, collections[k]))
        schema.set_collection(key)
    return schema
