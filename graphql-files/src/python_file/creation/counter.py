from __future__ import annotations

__all__ = ['Counter']


class Counter:
    def __init__(self):
        self.counters = dict()

    def inc_and_get(self, k: str) -> int:
        self.counters[k] = self.counters.get(k, 0) + 1
        return self.counters[k]

    def merge_counter(self, other: Counter) -> Counter:
        """
        Merge the other counter into this counter by keeping the highest value.
        :return: self
        """
        for k, v in other.counters.items():
            self.counters[k] = max(v, self.counters.get(k, 0))
        return self
