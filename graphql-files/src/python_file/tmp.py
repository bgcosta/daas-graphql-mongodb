import datetime
import json
import time

from creation.writers.graphql_java.writer import write

from creation.schema_creator import create_schema_from_db, create_schema_from_raw_data
from mongo_client import get_database


def create_from_raw():
    with open('../../../mongo-files/src/data.js') as f:
        db = json.loads(''.join(f.readlines()).split('=', 2)[1])
    db['lh'][0]['key']['test'].append(datetime.datetime.now())
    return create_schema_from_raw_data(db, collection_names=['lh'])


def create_from_remote_base():
    db = get_database(host='vm-docker2.cloud6.cantor.fr', db_name='legrand',
                      port=27117, user='legrand', pwd='qOVxM2hNwCbNWxNFU0QH')
    return create_schema_from_db(db)


if __name__ == '__main__':
    s = time.time()
    # schema = create_from_raw()
    schema = create_from_remote_base()
    e = time.time()
    print(f'{schema} - {e - s}')
    write(schema, 'GraphQLServer.java', 'schema.graphqls')
