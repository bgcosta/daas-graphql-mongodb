## Structure du dossier

```
+-- Dockerfile
+-- src
|    +-- bash_script
|    +-- python_file
|    |    +-- creation : dossier contenant les fichiers nécessaires à la création du server.  
|    |    |    +-- counter : Classe contenant des compteurs d'occurence de clés.  
|    |    |    +-- schema : Classe représentant le schéma utilisé par le serveur sous forme d'arbre.  
|    |    |    +-- schema_creator : Fonctions facilitant la création d'un schéma à partir de données.  
|    |    |    +-- writers : Differentes implémentation de l'écriture du serveur
|    |    |    |    +-- strawberry : Librairie python  
|    |    |    |    |    +-- writer : Fonction permettant d'écrire un schéma dans le fichier indiqué.  
|    |    |    |    |    +-- writer_template : Contient tous les patrons de classes et fonction du fichier écris.  
|    |    |    |    +-- graphql_java : Librairie java  
|    |    |    |    |    +-- writer : Fonction permettant d'écrire un schéma dans le fichier indiqué.  
|    |    |    |    |    +-- writer_template : Contient tous les patrons de classes et fonction du fichier écris.  
|    |    |    |    +-- utils : Fonction commune aux différentes implémentations  
|    |    |    |    +-- writer : Fonction permettant d'écrire un schéma dans le fichier indiqué.  
|    |    |    |    +-- writer_template : Contient tous les patrons de classes et fonction du fichier écris.  
|    |    +-- data_sample : dossier temporaire pour faire des tests.  
|    |    +-- patch : dossier contenant des corrections appliqué au module `strawberry`. Les modifications sont indiquées par un commentaire.  
|    |    |    +-- field : Ajout d'un paramètre dans l'appel au constructeur.  
|    |    |    +-- info : Ajout d'un champ dans la classe Info.  
|    |    +-- running : dossier contenant les fichiers nécessaires à l'execution du server.  
|    |    |    +-- node_processor : Extrait les données utiles de l'arbre graphql.  
|    |    |    +-- utils : Fonctions utilitaires pour le serveur.  
|    |    +-- arguments : Fichier de données contenant.  
|    |    +-- empty_server : Fichier serveur vide.
|    |    +-- mongo_client : Facilitation de la connection et des requêtes à la base MongoDB.  
```

## Todos

- Découpler l'implémentation en python et la création du serveur (différents modules et pas des modules liés)
- Fusion avec l'implémentation en java 
- Faire un vrai fichier de lancement de la création du serveur
- Garder un Docker uniquement pour la partie python
- Refaire l'arborescence du projet