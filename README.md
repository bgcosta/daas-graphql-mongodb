Le schéma produit par l'analyse des données venant de la base MongoDB peut se représenter sous forme d'un arbre. Dont:
 - Les nœuds sont des types (au sens GraphQL) ou des unions de types. 
 - Les feuilles sont des scalaires. (int, float, str, datetime, ...)
 - Les nœuds possèdent au minimum 1 fils.
 - Un nœud 'type' peut accéder à ses enfants via des arrêtes nommées. Les noms représentent les champs de la requête GraphQL.
 - Il y a des nœuds 'type' spéciaux: les alias de primitifs. Ils terminent par `Builtin` et ne possèdent qu'un seul fils: `_value`. Ces nœud ne sont présent qu'en tant qu'enfant d'une union.
 - Un nœud 'union' peut accéder à ses enfants via des arrêtes non nommées. 
 - Les nœuds union ne peuvent pas avoir plus d'un fils qui soit un type ([Démonstration](demonstration%201.md))
 - Certain nœuds type renvoient des listes, cela n'influe pas sur la structure de l'arbre.

Exemple d'arbre très abstrait: ![image](img/tree_template.png)

Les resolvers sont des fonctions et donc suivent les mêmes règles de nommages ([a-zA-Z\_][a-zA-Z\_0-9]*).
Cependant il y a quelques particularités:
 - Si le resolver contient un '\_' alors le caractère suivant sera mis en majuscule. Si c'était un caractère alpha-numérique, le '\_' sera supprimé.  
   Donc '\_name' -> 'Name' et '\_Name' -> 'Name' et '\_bonjour\_\_monsieur' -> 'Bonjour_Monsieur'.  
 - Si le resolver commence par '\_\_' alors le nom du type sera ajouté avant le reste du resolver mis en majuscule avec un '\_' en tant que séparateur.  
   Donc si le père est le type 'Customer': '\_\_name' -> 'Customer\_Name' et '\_\_\_name' -> 'Customer\_\_Name'


Les nœud alias de primitifs sont présents car il n'est pas possible d'avoir des unions contenant des scalaires. Donc on défini une seule fois un alias pour le scalaire voulut pour contourner cette restriction.  
Le nom de l'unique champ est important et passe outre la règle de nommage précédemment énoncée! Ce qui permet de remplacer les champs commençant par `_` par la valeur du scalaire dont ils sont l'alias. (Ce sont les seuls champs pouvant commencer par un `_`)