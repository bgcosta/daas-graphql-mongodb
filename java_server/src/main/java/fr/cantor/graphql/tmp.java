package fr.cantor.graphql;

import fr.cantor.graphql.runtime.utils.MongoClientRequester;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static fr.cantor.graphql.runtime.utils.FindIterableStream.stream;
import static java.util.stream.Collectors.toList;

public class tmp {
    public static void main(String[] args) {
        var projection = Set.of("_id");

//        var mongo = new MongoClientRequester("localhost", 27017, "test");
        var mongo = new MongoClientRequester("vm-docker2.cloud6.cantor.fr", 27117, "legrand", "legrand", "qOVxM2hNwCbNWxNFU0QH");

//        var v1 = stream(mongo.database.getCollection("lh").find()).collect(toList());
//        var v2 = stream(mongo.database.getCollection("lh").find().projection(new BsonDocument("1", new BsonBoolean(true)))).collect(toList());
//        System.out.println(v1);
//        System.out.println(v2);
        var cursor = mongo.find("ASSETS", Map.of(), projection);
        System.out.println(stream(cursor).collect(Collectors.toSet()));
    }
}
