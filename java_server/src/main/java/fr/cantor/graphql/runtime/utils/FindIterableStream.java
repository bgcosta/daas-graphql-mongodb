package fr.cantor.graphql.runtime.utils;

import com.mongodb.client.FindIterable;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class FindIterableStream<T> {
    private final FindIterable<T> it;

    public FindIterableStream(FindIterable<T> iterable) {
        this.it = iterable;
    }
    public Stream<T> stream() {
        return StreamSupport.stream(it.spliterator(), false);
    }
    public static <U> Stream<U> stream(FindIterable<U> iterable) {
        return new FindIterableStream<U>(iterable).stream();
    }
}
