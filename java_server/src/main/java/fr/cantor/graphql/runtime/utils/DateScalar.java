package fr.cantor.graphql.runtime.utils;

import graphql.language.StringValue;
import graphql.schema.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public final class DateScalar extends GraphQLScalarType {

    public DateScalar(String name) {
        super(name, "A Date", new DateCoercing());
    }

    private static final class DateCoercing implements Coercing<Date, String> {
        private static final DateTimeFormatter[] FORMATTERS = new DateTimeFormatter[]{
                DateTimeFormatter.ISO_DATE,
                DateTimeFormatter.ISO_DATE_TIME,
                DateTimeFormatter.ISO_LOCAL_DATE,
                DateTimeFormatter.ISO_INSTANT
        };

        @Override
        public String serialize(Object input) throws CoercingSerializeException {
            if (input instanceof String) return (String) input;
            if (!(input instanceof Date date))
                throw new CoercingSerializeException("Invalid value type " + input);
            return DateTimeFormatter.ISO_DATE_TIME.format(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }

        @Override
        public Date parseValue(Object input) throws CoercingParseValueException {
            if (input instanceof Date) return (Date) input;
            if (!(input instanceof String formatted))
                throw new CoercingParseValueException("Invalid value type " + input);
            for (var formatter : FORMATTERS) {
                try {
                    LocalDate date = LocalDate.from(formatter.parse(formatted));
                    return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
                } catch (DateTimeParseException ignored) {}
            }
            throw new CoercingParseValueException("Invalid value type " + input);
        }

        @Override
        public Date parseLiteral(Object input) throws CoercingParseLiteralException {
            if (input instanceof Date) return (Date) input;
            if (!(input instanceof StringValue))
                throw new CoercingParseLiteralException("Invalid value type " + input);
            return parseValue(((StringValue) input).getValue());
        }
    }
}
