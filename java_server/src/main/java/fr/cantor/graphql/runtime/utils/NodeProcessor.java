package fr.cantor.graphql.runtime.utils;

import graphql.language.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Todo : Convert to visitor (later)
public record NodeProcessor(NodeProcessorData data) {
    private static final String IGNORE_TOKEN = "$";

    public static record NodeProcessorResult(Set<? extends String> projection,
                                             Map<? extends String, ? extends Map<String, ?>> filters) {
        private static final NodeProcessorResult EMPTY = new NodeProcessorResult(Set.of(), Map.of());
        public static NodeProcessorResult empty() {
            return EMPTY;
        }
    }

    public record NodeProcessorData(Map<? super String, ?> variables,
                                    Map<? super String, String> aliases,
                                    Map<? super String, ? extends FragmentDefinition> fragments,
                                    Map<? super String, NodeProcessorResult> resolvedFragments) {
        public static NodeProcessorData create(Map<? super String, ?> variables,
                                               Map<? super String, String> aliases,
                                               Map<? super String, ? extends FragmentDefinition> fragments) {
            return new NodeProcessorData(variables, aliases, fragments, new HashMap<>());
        }
    }

    public NodeProcessorResult processNode(Node<?> node) {
        if (node instanceof Field fieldNode) {
            return onFieldNode(fieldNode, "", true);
        }
        return NodeProcessorResult.empty();
    }

    private String getAlias(NamedNode<?> node) {
        var name = node.getName();
        if (name.equalsIgnoreCase("_value")) return IGNORE_TOKEN;
        return data.aliases.getOrDefault(name, name);
    }

    private NodeProcessorResult onSelectionSetNode(SelectionSet node, String path) {
        if (node == null) return NodeProcessorResult.empty();

        Set<String> projection = new HashSet<>();
        Map<String, Map<String, ?>> filters = new HashMap<>();
        for (var n : node.getSelections()) {
            var npr = onSelectionNode(n, path);
            projection.addAll(npr.projection);
            npr.filters.forEach(filters::put);
        }
        return new NodeProcessorResult(projection, filters);
    }

    private NodeProcessorResult onSelectionNode(Selection<?> node, String path) {
        if (node instanceof Field field) return onFieldNode(field, path, false);
        if (node instanceof InlineFragment inlineFragment) return onInlineFragmentNode(inlineFragment, path);
        if (node instanceof FragmentSpread fragmentSpread) return onFragmentSpreadNode(fragmentSpread, path);
        return NodeProcessorResult.empty();
    }

    private Optional<? extends String> prefixWithNodeName(String name, String nodeName) {
        if (name.equals(IGNORE_TOKEN)) return Optional.of(nodeName);
        if (name.equals("__typename")) return Optional.empty();
        return Optional.of(nodeName + "." + name);
    }

    private Set<? extends String> prefixWithNodeName(NodeProcessorResult npr, String name) {
        if (npr.projection.size() == 0) return Set.of(name);
        if (npr.projection.contains(IGNORE_TOKEN)) return Set.of(name);
        return npr.projection.stream().flatMap(s -> prefixWithNodeName(s, name).stream()).collect(Collectors.toSet());
    }

    private <T extends Map<?, ?>> Optional<Map.Entry<String, T>> prefixKeyWithNodeName(
            Map.Entry<? extends String, T> entry, String name) {
        var key = prefixWithNodeName(entry.getKey(), name);
        System.out.println(key  + " - " + entry  + " - " + name);
        if (key.isEmpty()) return Optional.empty();
        if (entry.getValue().size() == 0) return Optional.empty();
        return Optional.of(Map.entry(key.get(), entry.getValue()));
    }

    private NodeProcessorResult onFieldNode(Field node, String path, boolean isRoot) {
        // Collection don't have arguments
        if (isRoot) return onSelectionSetNode(node.getSelectionSet(), path);

        String name = getAlias(node);
        path += name + "/";
        var npr = onSelectionSetNode(node.getSelectionSet(), path);
        var args = Stream.concat(npr.filters.entrySet().stream(),
                                 onArgumentNodes(node.getArguments(), path, name).entrySet().stream())
                .flatMap(entry -> prefixKeyWithNodeName(entry, name).stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return new NodeProcessorResult(prefixWithNodeName(npr, name), args);
    }

    private Map<? extends String, ? extends Map<String, ?>> onArgumentNodes(List<? extends Argument> nodes, String path, String name) {
        Map<String, Object> filters = new HashMap<>();
        for (var node : nodes) {
            var nodeName = node.getName();
            var key = path.replaceAll("/$", "." + nodeName);
            if (Filters.ALL_FILTERS.containsKey(nodeName)) {
                filters.put(Filters.ALL_FILTERS.get(nodeName), onValueNode(node.getValue(), key));
            }
        }
        return Map.of(name, filters);
    }

    private NodeProcessorResult onFragmentSpreadNode(FragmentSpread node, String path) {
        String name = getAlias(node);
        return data.resolvedFragments.computeIfAbsent(name, k -> onFragmentDefinitionNode(data.fragments.get(k), path));
    }

    private NodeProcessorResult onInlineFragmentNode(InlineFragment node, String path) {
        return onSelectionSetNode(node.getSelectionSet(), path);
    }

    private NodeProcessorResult onFragmentDefinitionNode(FragmentDefinition node, String path) {
        String name = getAlias(node);
        var npr = onSelectionSetNode(node.getSelectionSet(), path);
        var projection = prefixWithNodeName(npr, name);
        return new NodeProcessorResult(projection, npr.filters);
    }

    private Object onValueNode(Value<?> node, String key) {
        if (node instanceof VariableReference) return data.variables.get(key);
        if (node instanceof IntValue value) return value.getValue();
        if (node instanceof FloatValue value) return value.getValue();
        if (node instanceof StringValue value) return value.getValue();
        if (node instanceof BooleanValue value) return value.isValue();
        // For any other value [ArrayValue; ObjectValue; NullValue; EnumValue]
        return null;
    }

}
