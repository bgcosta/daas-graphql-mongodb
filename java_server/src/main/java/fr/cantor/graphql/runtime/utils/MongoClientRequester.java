package fr.cantor.graphql.runtime.utils;

import com.mongodb.client.*;
import graphql.language.BooleanValue;
import org.bson.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class MongoClientRequester {
    public final MongoDatabase database;
    public final MongoClient client;

    public MongoClientRequester(String host, int port, String dbName) {
        this.client = MongoClients.create(String.format("mongodb://%s:%d", host, port));
        this.database = client.getDatabase(dbName);
    }

    public MongoClientRequester(String host, int port, String dbName, String user, String pwd) {
        this.client = MongoClients.create(
                String.format("mongodb://%s:%s@%s:%d/?authSource=%s", user, pwd, host, port, dbName)
        );
        this.database = client.getDatabase(dbName);
    }

    public static MongoClientRequester createFromEnv() {
        var dbName = System.getenv("MONGODB_DB_NAME");
        var host = System.getenv("MONGODB_HOST");
        var port = System.getenv("MONGODB_PORT");
        var user = System.getenv("MONGODB_USER");
        var pwd = System.getenv("MONGODB_PWD");
        if (host == null || port == null || dbName == null) {
            throw new IllegalStateException("Missing one of these environment variable : " +
                    "[MONGODB_DB_NAME, MONGODB_HOST, MONGODB_PORT]");
        }
        if (user == null && pwd == null) {
            return new MongoClientRequester(host, Integer.parseInt(port), dbName);
        }
        return new MongoClientRequester(host, Integer.parseInt(port), dbName, user, pwd);
    }

    private static Optional<? extends BsonValue> objectToBsonValue(Object obj) {
        if (obj instanceof String value) return Optional.of(new BsonString(value));
        if (obj instanceof BigInteger value) return Optional.of(new BsonInt64(value.longValue()));
        if (obj instanceof BigDecimal value) return Optional.of(new BsonDouble(value.doubleValue()));
        if (obj instanceof Boolean value) return Optional.of(new BsonBoolean(value));
        // Fail-safe
        if (obj instanceof Short value) return Optional.of(new BsonInt32(value));
        if (obj instanceof Integer value) return Optional.of(new BsonInt64(value));
        if (obj instanceof Long value) return Optional.of(new BsonInt64(value));
        if (obj instanceof Float value) return Optional.of(new BsonDouble(value));
        if (obj instanceof Double value) return Optional.of(new BsonDouble(value));
        return Optional.empty();
    }

    public FindIterable<? extends Document> find(String collectionName,
                                                 Map<? extends String, ? extends Map<? extends String, ?>> filters) {
        var collection = database.getCollection(collectionName);
        var bsonFilters = new BsonDocument();
        for (var entry : filters.entrySet()) {
            var subDoc = new BsonDocument();
            entry.getValue().forEach((k, v) ->
                    objectToBsonValue(v).ifPresent(x -> subDoc.append(k, x)));
            bsonFilters.append(entry.getKey(), subDoc);
        }
        System.out.println("filters"  + " - " + bsonFilters);
        return collection.find(bsonFilters);
    }

    public FindIterable<? extends Document> find(String collectionName,
                                                 Map<? extends String, ? extends Map<? extends String, ?>> filters,
                                                 Set<? extends String> projection) {
        var findIterable = find(collectionName, filters);
        var doc = new BsonDocument();
        doc.append("_id", new BsonBoolean(false));
        projection.forEach(s -> doc.append(s, new BsonBoolean(true)));

        System.out.println("projection"  + " - " + doc);
        return findIterable.projection(doc);
    }
}
