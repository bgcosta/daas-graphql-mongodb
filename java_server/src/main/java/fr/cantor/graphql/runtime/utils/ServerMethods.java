package fr.cantor.graphql.runtime.utils;

import fr.cantor.graphql.runtime.utils.NodeProcessor.NodeProcessorData;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.idl.TypeRuntimeWiring;
import org.bson.Document;

import java.util.Map;
import java.util.stream.Collectors;

import static fr.cantor.graphql.runtime.utils.FindIterableStream.stream;

public final class ServerMethods {

    private ServerMethods() {}

    public static Map<? super String, ?> getVariables(DataFetchingEnvironment fetcher) {
        return fetcher.getSelectionSet()
                .getArguments()
                .entrySet()
                .stream()
                .flatMap(e ->
                        e.getValue()
                                .entrySet()
                                .stream()
                                .map(e2 ->
                                        Map.entry(e.getKey() + "." + e2.getKey(), e2.getValue())
                                )
                ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
    public static NodeProcessorData getNodeProcessorData(DataFetchingEnvironment fetcher,
                                                         Map<String, String> aliases) {
        return NodeProcessorData.create(
                getVariables(fetcher),
                aliases,
                fetcher.getExecutionContext().getFragmentsByName()
        );
    }

    public static TypeRuntimeWiring newTypeWiring(String type, Map<String, String> aliases, String... fields) {
        var wiring = TypeRuntimeWiring.newTypeWiring(type);
        for (var field : fields)
            wiring.dataFetcher(field, fetcher ->
                    ((Document) fetcher.getSource()).get(aliases.getOrDefault(field, field)));
        return wiring.build();
    }
    public static TypeRuntimeWiring newBuiltinWiring(String builtin) {
        return TypeRuntimeWiring.newTypeWiring(builtin)
                .dataFetcher("_value", DataFetchingEnvironment::getSource)
                .build();
    }

    public static record UnionData(Class<?> clazz, String type) {}

    public static TypeRuntimeWiring newUnionWiring(String union, UnionData... types) {
        return TypeRuntimeWiring.newTypeWiring(union)
                .typeResolver(env -> {
                    var obj = env.getObject();
                    for (var type : types)
                        if (type.clazz.isInstance(obj))
                            return env.getSchema().getObjectType(type.type);
                    return null;
                })
                .build();
    }

    public static TypeRuntimeWiring newQueryWiring(String query, Map<String, String> aliases,
                                                   MongoClientRequester client, String... collections) {
        var wiring = TypeRuntimeWiring.newTypeWiring(query);
        for (var collection : collections) {
            wiring.dataFetcher(collection, fetcher -> {
                var processor = new NodeProcessor(getNodeProcessorData(fetcher, aliases));
                var npr = processor.processNode(fetcher.getField());
                return stream(client.find(collection, npr.filters(), npr.projection()))
                        .collect(Collectors.toList());
            });
        }
        return wiring.build();
    }

}
