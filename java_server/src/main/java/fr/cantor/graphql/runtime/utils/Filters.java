package fr.cantor.graphql.runtime.utils;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Filters {

    private Filters() {}

    public static final Map<String, String> COMPARISON_FILTER = Map.of(
            "gt", "$gt",
            "gte", "$gte",
            "lt", "$lt",
            "lte", "$lte",
            "eq", "$eq",
            "ne", "$ne"
    );
    public static final Map<String, String> STRING_FILTER = Map.of(
            "like", "$regex"
    );
    public static final Map<String, String> ALL_FILTERS =
            Stream.concat(STRING_FILTER.entrySet().stream(), COMPARISON_FILTER.entrySet().stream())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
}
