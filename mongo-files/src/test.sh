#!/usr/bin/env bash

mongod --logpath /var/log/mongodb.log --dbpath /data/db &
sleep "${DELAY_BEFORE_START}" &&
mongo 127.0.0.1:27017 insert_data.js

mongod --logpath /var/log/mongodb.log --dbpath /data/db