data = {
    "person": [{
        "_id": "17",
        "person": {
            "name": {
                "first": "Jo",
                "last": "Scar",
                "surname": "Scarlett Johansson"
            },
            "age": 46.0,
            "family": [
                {
                    "age": 17.0,
                    "name": "Bob Henderson",
                    "link": "brother",
                    "special": {
                        "hair": "brown",
                        "fur": "grey"
                    }
                },
                {
                    "age": 4.0,
                    "name": "Rebel",
                    "link": "cat",
                    "special": {
                        "breed": "Ragdoll",
                        "fur": [
                            {
                                "color": "white"
                            },
                            {
                                "color": "brown"
                            },
                            {
                                "color": "grey"
                            }
                        ]
                    }
                },
                {
                    "age": 75.0,
                    "name": "Laetitia",
                    "link": "mother",
                    "special": {
                        "hair": "salt n pepper"
                    }
                }
            ]
        }
    }],
    "obj": [{
        "id": 10.0,
        "foo": "bob",
        "attrs": [
            {
                "id": 2.0,
                "foo": "ok",
                "bar": "bar"
            },
            {
                "id": "toto",
                "foo": 3.0,
                "boz": "boz"
            },
            {
                "foo": [
                    {
                        "foo": "toto",
                        "inner": [
                            {
                                "id": "a"
                            },
                            {
                                "id": 1.0
                            }
                        ]
                    },
                    {
                        "foo": 3.0,
                        "inner": [
                            {
                                "id": "b"
                            },
                            {
                                "id": 2.0
                            }
                        ]
                    }
                ]
            }
        ]
    }],
    "test": [{
        "id": "10",
        "name": {
            "_value": 10.0,
            "surname": [
                {"x": 10.0},
                {"x": "12"}
            ]
        }
    }, {
        "id": "11",
        "name": {
            "first": "Jo",
            "last": "Scar",
            "surname": [
                {"x": "14"},
                {"x": 15.0}
            ]
        }
    }],
    "lg": [{
        "id": {
            "value": 30.0,
            "key": "&**"
        },
        "0": "a",
        "1": "b",
        "2": "n"
    }],
    "lh": [{
            "key": {"test": [{"u": 3.0}, "a"]},
            "1": 14.0
        },
        {
            "key": {"test": [{"u": 4.0}, "c"]},
            "1": 13.0
        }
    ]
}