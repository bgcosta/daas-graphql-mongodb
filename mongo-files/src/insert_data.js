load('data.js')

const conn = new Mongo();
const db = conn.getDB("test");

data.person.forEach(x => db.person.save(x));
data.obj.forEach(x => db.obj.save(x));
data.test.forEach(x => db.test.save(x));
data.lg.forEach(x => db.lg.save(x));
data.lh.forEach(x => db.lh.save(x));

db.lh.save({
            "key": {"test": [new Date("2012-04-23T18:25:43.511Z")]},
            "1": 12.0
        })

db.lh.find().forEach(printjson);
db.getSiblingDB("admin").shutdownServer({ "force" : true });