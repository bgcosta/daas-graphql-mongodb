Environment variables used in the docker.

Need to have at least:  
    MONGODB_HOST,  
    MONGODB_PORT,  
    MONGODB_DB_NAME,  
    DELAY_BEFORE_START,  
    AUTO_UPDATE_DELAY,  